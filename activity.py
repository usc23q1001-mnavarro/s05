from abc  import ABC, abstractclassmethod

class Animal(ABC):
	@abstractclassmethod
	def eat(self, food):
		pass
	def make_sound(self):
		pass

class Cat(Animal):
	def __init__(self, name, breed, age):
		super().__init__()
		self._name = name
		self._breed = breed
		self._age = age


	#setter
	def set_name(self, name):
		self._name = name
	def set_breed(self, breed):
		self._breed = breed
	def set_age(self, age):
		self._age = age

	#getter
	def call(self):
		print(f'{self._name}, come on!')
	# def get_breed(self):
	# 	print(f'Name of cat: {self._name}')
	# def get_name(self):
	# 	print(f'Name of cat: {self._name}')	

	#inherited the Animal class
	def eat(self, food):
		print(f'Serve me {food}')
	def make_sound(self):
		print("Miaow! Nyaw! Nyaaaa!")

class Dog(Animal):
	def __init__(self, name, breed, age):
		super().__init__()
		self._name = name
		self._breed = breed
		self._age = age


	#setter
	def set_name(self, name):
		self._name = name
	def set_breed(self, breed):
		self._breed = breed
	def set_age(self, age):
		self._age = age

	#getter
	def call(self):
		print(f'Here {self._name}')

	#inherited the Animal class
	def eat(self, food):
		print(f'Eaten {food}')
	def make_sound(self):
		print("Bark! Woof! Arf!")



# Test cases
dog1 = Dog("Isis", "Dalmatian", 15)
dog1.eat("steak")
dog1.make_sound()
dog1.call()

cat1 = Cat("Puss", "Persian", 4)
cat1.eat("Tuna")
cat1.make_sound()
cat1.call()
